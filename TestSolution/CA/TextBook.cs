﻿using System;

namespace CA
{
    public class TextBook
    {
        /*private int numberOfPages;

        public int NumberOfPages
        {
            get { return numberOfPages;}
            set { numberOfPages = value; }
        }*/
        public int NumberOfPages { get; set; }
        public string Name { get; set; }

        public string ToCsv()
        {
            return Name + ";" + NumberOfPages;
        }

        // public string GetInfo()
        // {
        //     string print = $"Title: {Name} (#{NumberOfPages})";
        //     return print;
        // }
    }
}