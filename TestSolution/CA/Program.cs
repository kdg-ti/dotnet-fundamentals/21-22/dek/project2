﻿using System;
using CA.Extensions;

namespace CA
{
    class Program
    {
        static void Main(string[] args)
        {
            TextBook book = new TextBook() { NumberOfPages = 210, Name = ".NET Basics" };
            Console.WriteLine(book.GetInfo());
            Console.WriteLine(PrintBook(book));
            Console.WriteLine(book.ToCsv());
        }

        static string PrintBook(TextBook book)
        {
            string print = $"Title: {book.Name} (#{book.NumberOfPages})";
            return print;
        }
    }
}