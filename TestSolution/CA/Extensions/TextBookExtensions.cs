﻿namespace CA.Extensions
{
    public static class TextBookExtensions
    {
        public static string GetInfo(this TextBook book)
        {
            string print = $"Title: {book.Name} (#{book.NumberOfPages})";
            return print;
        }
    }
}